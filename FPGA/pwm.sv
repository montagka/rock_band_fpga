// Kaden Montague
// PWM Controller

module pwm (
  input           clk,
  input           reset,
  input           mode,

  output logic    pwm
);

logic [7:0]      counter;
logic [7:0]      compare;

always_comb begin
  unique case (mode)
    1'd1: compare = 8'd16;
    1'd0: compare = 8'd24;
  endcase
end

always_ff @(posedge clk, posedge reset)
  if (reset) counter <= 8'd0;
  else       counter <= counter + 1'd1;

assign pwm = (counter <= compare);

endmodule // pwm