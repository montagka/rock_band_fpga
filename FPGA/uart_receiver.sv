// UART receiver

module uart_receiver (
  input uart_data,
  input clk,
  input reset_n,
  output logic [7:0] uart_data_out
);

logic uart_data_ready;

enum logic [3:0] {
  NO_DATA = 4'b0001,
  EDGE    = 4'b0010,
  DATA    = 4'b0100,
  DONE    = 4'b1000
} uart_rec_ps, uart_rec_ns;

logic [3:0] fast_cnt_ps;
logic [2:0] data_bit_cnt_ps;

logic [3:0] fast_cnt_ns;
logic [2:0] data_bit_cnt_ns;

logic [7:0] uart_data_incoming;

logic data_in_reg_enable;

assign data_in_reg_enable = ((fast_cnt_ps == 4'hf) && (uart_rec_ps == DATA));

shift_reg_reverse #(.width(8)) shift_reg_0(
  .reset_n        (reset_n            ),
  .clk            (clk                ),
  .data_ena       (data_in_reg_enable ),
  .serial_data    (uart_data          ),
  .parallel_data  (uart_data_incoming )
);

//
// PS <- NS on clk.
//
always_ff @(posedge clk, negedge reset_n)
  if (!reset_n) uart_rec_ps <= NO_DATA;
  else          uart_rec_ps <= uart_rec_ns;

always_comb begin
  uart_data_ready = 1'b0;
  unique case (uart_rec_ps)
    NO_DATA: begin
      if (!uart_data) uart_rec_ns = EDGE;
      else            uart_rec_ns = uart_rec_ps;
    end

    EDGE:    begin
      if (fast_cnt_ps == 4'hf) begin
        if (!uart_data)
          uart_rec_ns = DATA;
        else
          uart_rec_ns = NO_DATA;
      end
      else uart_rec_ns = uart_rec_ps;
    end

    DATA: begin
      if ((fast_cnt_ps == 4'hf) && (data_bit_cnt_ps == 3'h7))
        uart_rec_ns = DONE;
      else uart_rec_ns = uart_rec_ps;
    end

    DONE: begin
      if (uart_data) begin
        uart_data_ready = 1'b1;
        uart_rec_ns = NO_DATA;
      end
      else
        uart_rec_ns = uart_rec_ps;
    end
  endcase
end

always_ff @(posedge clk, negedge reset_n)
  if      (!reset_n)                uart_data_out <= 8'b0;
  else if (uart_data_ready)         uart_data_out <= uart_data_incoming;

always_ff @(posedge clk, negedge reset_n)
  if (!reset_n) fast_cnt_ps <= 4'h9;
  else          fast_cnt_ps <= fast_cnt_ns;

always_comb begin
  case (fast_cnt_ps)
    4'h9: begin
      if ((uart_rec_ps == EDGE) || (uart_rec_ps == DATA))
        fast_cnt_ns = fast_cnt_ps + 1'b1;
      else
        fast_cnt_ns = fast_cnt_ps;
    end

    4'hf: begin
      if ((uart_rec_ps == EDGE) && (uart_data))
        fast_cnt_ns = 4'h9;
      else
        fast_cnt_ns = 4'h0;
    end

    default: fast_cnt_ns = fast_cnt_ps + 1'b1;
  endcase
end



always_ff @(posedge clk, negedge reset_n)
  if (!reset_n) data_bit_cnt_ps <= 3'h0;
  else          data_bit_cnt_ps <= data_bit_cnt_ns;

always_comb begin
  if ((fast_cnt_ps == 4'hf) && (uart_rec_ps == DATA))
    data_bit_cnt_ns = data_bit_cnt_ps + 1'b1;
  else
    data_bit_cnt_ns = data_bit_cnt_ps;
end

endmodule // uart_receiver
