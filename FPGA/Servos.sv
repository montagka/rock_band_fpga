// Kaden Montague
// PWM Controller

module Servos (
  input           clk_50m,   // 
  input           reset,     
  //input           [4:0] mode,   
  input           uart_data_in,

  output          [4:0] pwm,  
  output          [7:0] leds   
);

wire clk_12k;
wire clk_10k;
wire clk_160;
logic [7:0] uart_data_from_pc;
logic [4:0] mode;

assign leds = uart_data_from_pc;

// soft_clock #(.CYCLE(100000)) sc_2 (
//   .clk (clk_12k)
// );


//
// Pwm controllers output one of two PWM waveforms (randomly chosen positions.)
//
pwm pwm_controller_1 (
  .clk                (clk_12k        ),
  .reset              (!reset         ),
  .mode               (mode[0]        ),
  .pwm                (pwm[0]         )
  );

pwm pwm_controller_2 (
  .clk                (clk_12k        ),
  .reset              (!reset         ),
  .mode               (mode[1]        ),
  .pwm                (pwm[1]         )
  );

pwm pwm_controller_3 (
  .clk                (clk_12k        ),
  .reset              (!reset         ),
  .mode               (mode[2]        ),
  .pwm                (pwm[2]         )
  );

pwm pwm_controller_4 (
  .clk                (clk_12k        ),
  .reset              (!reset         ),
  .mode               (mode[3]        ),
  .pwm                (pwm[3]         )
  );

pwm pwm_controller_5 (
  .clk                (clk_12k        ),
  .reset              (!reset         ),
  .mode               (mode[4]        ),
  .pwm                (pwm[4]         )
  );

pll pll_inst0(
  .inclk0   (clk_50m), // input clock to PLL module
  .c0       (clk_12k), // Various output clocks
  .c1       (       )
);

plluart pll_inst1(
  .inclk0   (clk_50m),
  .c0       (clk_10k),
  .c1       (clk_160)
);

logic [7:0] raw_num_from_uart;

assign raw_num_from_uart = uart_data_from_pc - 8'd48; // ascii 0 = d48

assign mode = raw_num_from_uart[4:0];

uart_receiver rec_0(
  .uart_data  (uart_data_in),         // Single bit data from PC
  .clk        (clk_160     ),         // 16 * baud rate
  .reset_n    (reset       ),         // active low reset
  .uart_data_out (uart_data_from_pc ) // parallel data from PC
);

endmodule // Servos