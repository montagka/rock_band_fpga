// uart transmitter
// Kaden Montague
// ECE 474 Lab6/7?

module uart_xmitter (
  input               clk,
  input               reset_n,
  input               adc_data_ready,
  input        [3:0]  digit_one,
  input        [3:0]  digit_two,
  input        [3:0]  digit_three,
  input        [3:0]  digit_four,
  output logic        data_to_uart
);

logic [1:0] dig_mux_sel;
logic [1:0] char_mux_sel;

enum logic [3:0]{
  CHAR_0 = 4'b1100,
  CHAR_1 = 4'b1101,
  CHAR_2 = 4'b1000,
  CHAR_3 = 4'b0100,
  CHAR_4 = 4'b0000,
  CHAR_5 = 4'b1110,
  CHAR_6 = 4'b1111
} char_cntr_ps, char_cntr_ns;

logic [3:0] bit_cntr_ps;
logic [3:0] bit_cntr_ns;

enum logic {
  DONE  = 1'b0,
  START = 1'b1
} uart_status_ps, uart_status_ns;

//
// PS -< NS on clk
//
always_ff @(posedge clk, negedge reset_n)
  if (!reset_n) char_cntr_ps <= CHAR_6;
  else          char_cntr_ps <= char_cntr_ns;

always_comb begin
  dig_mux_sel   = char_cntr_ps[3:2];
  char_mux_sel  = char_cntr_ps[1:0];
  unique case (char_cntr_ps)
    CHAR_6 :
      begin  
        if ((bit_cntr_ps == 4'd9) && uart_status_ps == START)
          char_cntr_ns = CHAR_0;
        else
          char_cntr_ns = char_cntr_ps;;
      end

    CHAR_0 :
      begin
        if (bit_cntr_ps == 4'd9)
          char_cntr_ns = CHAR_1;
        else
          char_cntr_ns = char_cntr_ps;
      end

    CHAR_1 :
      begin
        if (bit_cntr_ps == 4'd9)
          char_cntr_ns = CHAR_2;
        else
          char_cntr_ns = char_cntr_ps;
      end

    CHAR_2 :
      begin
        if (bit_cntr_ps == 4'd9)
          char_cntr_ns = CHAR_3;
        else
          char_cntr_ns = char_cntr_ps;
      end

    CHAR_3 :
      begin
        if (bit_cntr_ps == 4'd9)
          char_cntr_ns = CHAR_4;
        else
          char_cntr_ns = char_cntr_ps;
      end

    CHAR_4 :
      begin
        if (bit_cntr_ps == 4'd9)
          char_cntr_ns = CHAR_5;
        else
          char_cntr_ns = char_cntr_ps;
      end

    CHAR_5 :
      begin
        if (bit_cntr_ps == 4'd9)
          char_cntr_ns = CHAR_6;
        else
          char_cntr_ns = char_cntr_ps;  
      end 
  endcase // char_cntr_ps
end

//
// PS <- NS on clk.
//
always_ff @(posedge clk, negedge reset_n)
  if (!reset_n) bit_cntr_ps <= 4'h9;
  else          bit_cntr_ps <= bit_cntr_ns;

//
// Increment bit count always once the byte sending has started.
//
always_comb begin
  case (bit_cntr_ps)
    4'h9: begin
      if (uart_status_ps == START)  bit_cntr_ns = 4'b0;
      else                          bit_cntr_ns = bit_cntr_ps;
    end

    default:                        bit_cntr_ns = bit_cntr_ps + 1'b1;
  endcase
end

//
// PS <- NS on clk.
//
always_ff @(posedge clk, negedge reset_n)
  if (!reset_n) uart_status_ps <= DONE;
  else          uart_status_ps <= uart_status_ns;


always_comb begin
  unique case (uart_status_ps)
    DONE: begin
      if (adc_data_ready) uart_status_ns = START;
      else                uart_status_ns = uart_status_ps;
    end

    START: begin
      if ((char_cntr_ps == CHAR_6) && (bit_cntr_ps == 4'd8))
        uart_status_ns = DONE;
      else
        uart_status_ns = uart_status_ps;
    end
  endcase
end

logic [3:0] bcd_to_send;
logic [7:0] ascii_to_send;
logic [7:0] output_char;

always_comb begin
  unique case (dig_mux_sel)
    2'd0 : bcd_to_send = digit_one;
    2'd1 : bcd_to_send = digit_two;
    2'd2 : bcd_to_send = digit_three;
    2'd3 : bcd_to_send = digit_four;
  endcase // dig_mux_sel
end

assign ascii_to_send = bcd_to_send + 8'd48;

always_comb begin
  unique case (char_mux_sel)
    2'd0 : output_char = ascii_to_send;
    2'd1 : output_char = 8'd46;
    2'd2 : output_char = 8'd13;
    2'd3 : output_char = 8'd10;
  endcase // char_mux_sel
end

always_comb begin
  unique case (bit_cntr_ps)
    4'd0 : data_to_uart = 1'b0;
    4'd1 : data_to_uart = output_char[0];
    4'd2 : data_to_uart = output_char[1];
    4'd3 : data_to_uart = output_char[2];
    4'd4 : data_to_uart = output_char[3];
    4'd5 : data_to_uart = output_char[4];
    4'd6 : data_to_uart = output_char[5];
    4'd7 : data_to_uart = output_char[6];
    4'd8 : data_to_uart = output_char[7];
    4'd9 : data_to_uart = 1'b1;
  endcase // bit_cntr_ps
end

endmodule // uart_xmitter